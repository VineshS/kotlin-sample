package com.example.kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val i = intent
        val name = i.getStringExtra("Name")
        val phone = i.getIntExtra("Phone",0)

        nameSecondActivityTextView.text = name.toString()
        phoneSecondActivityTextView.text = phone.toString()

        logoutSecondActivityButton.setOnClickListener {
            startActivity(Intent(this,LoginActivity::class.java))
        }


    }
}