package com.example.kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        submitMainActivityButton.setOnClickListener {
            var name:String = nameMainActivityEditText.text.toString()
            var phone:Int = phoneMainActivityPhoneText.text.toString().toInt()

            val intent = Intent(this,SecondActivity::class.java)
            intent.putExtra("Name",name)
            intent.putExtra("Phone", phone)

            startActivity(intent)
        }

    }


}